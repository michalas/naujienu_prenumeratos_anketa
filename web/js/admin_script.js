/**
 * Created by michal on 23/10/17.
 */

$(document).ready(function () {

    //activation of BootStrap tooltips
    $('[data-toggle="tooltip"]').tooltip();

    //ID "eye" column toggle button action
    $("#id-column-control").click(function () {
        $(".id-column").toggle("slow");
    });


    //DataTabes initialization
    $('#table').DataTable({
        "columnDefs": [{
            "targets": [-2, -1, 0,1 ],
            "orderable": false
        }],
        responsive: true,
        autoWidth: false,
        language: {
            "sEmptyTable":      "Lentelėje nėra duomenų",
            "sInfo":            "Rodomi įrašai nuo _START_ iki _END_ iš _TOTAL_ įrašų",
            "sInfoEmpty":       "Rodomi įrašai nuo 0 iki 0 iš 0",
            "sInfoFiltered":    "(atrinkta iš _MAX_ įrašų)",
            "sInfoPostFix":     "",
            "sInfoThousands":   " ",
            "sLengthMenu":      "Rodyti _MENU_ įrašus",
            "sLoadingRecords":  "Įkeliama...",
            "sProcessing":      "Apdorojama...",
            "sSearch":          "Ieškoti:",
            "sThousands":       " ",
            "sUrl":             "",
            "sZeroRecords":     "Įrašų nerasta",

            "oPaginate": {
                "sFirst": "Pirmas",
                "sPrevious": "Ankstesnis",
                "sNext": "Tolimesnis",
                "sLast": "Paskutinis"
            }
        }
    });


});