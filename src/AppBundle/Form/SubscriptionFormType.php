<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 20/10/17
 * Time: 11:06
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class is fully based on Symfony Form Component
 * Builds, validates and submits form at /subscription page.
 *
 * @package AppBundle\Form
 */
class SubscriptionFormType extends AbstractType {


	/**
	 * Form building (from Symfony Examples)
	 *
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm( FormBuilderInterface $builder, array $options ) {

		//load and decode .json file
		$decoded_json = json_decode( file_get_contents( __DIR__ . "/../../../app/Resources/categories.json" ), true );

		//getting categories .json array values
		$categories_json = array_values( $decoded_json );

		//setuping form with all it's rules and validation parameters
		$builder
			->add( 'name', TextType::class, [
				'attr'        => array( 'placeholder' => 'įveskite vardą' ),
				'required'    => true,
				'label'       => 'Jūsų vardas:',
				'constraints' => array(
					new NotBlank( array(
						'message' => "*Šis laukas negali būti tuščias."
					) ),
					new Length( array(
						'min'        => 2,
						'minMessage' => '*Per mažai simbolių. Patikrinkite ar tikrai įvedėte teisingą vardą?',
						'max'        => 100,
						'maxMessage' => '*Per daug simbolių. Ar tikrai įvedėte teisingą vardą?'
					) )
				)
			] )
			->add( 'email', EmailType::class, [
				'attr'        => array( 'placeholder' => 'įveskite el. paštą' ),
				'required'    => true,
				'label'       => 'El. pašto adresas:',
				'constraints' => array(
					new NotBlank( array( 'message' => "*Šis laukas negali būti tuščias." ) )
				)
			] )
			->add( 'categories', ChoiceType::class, [
				'label'        => 'Pasirinkite Jus dominančias naujienų kategorijas:',
				'choices'      => $categories_json,
				'choice_label' => function ( $value, $key, $index ) {
					return $value;
				},
				'multiple' => true,
				'expanded' => true
			] )
			->add( 'submit', SubmitType::class, [
			'attr'  => array( 'class' => 'btn-primary' ),
			'label' => 'Registruotis'
		] );


	}


	public function configureOptions( OptionsResolver $resolver ) {
	}

	public function getBlockPrefix() {
		return 'app_bundle_subscription_form_type';
	}
}
