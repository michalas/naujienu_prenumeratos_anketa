<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 20/10/17
 * Time: 12:10
 */

namespace AppBundle\Controller;

use AppBundle\Form\SubscriptionFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 *
 * Class publicController
 * Class owns all public part methods
 *
 * @package AppBundle\Controller
 */
class publicController extends Controller {


	/**
	 * @Route("/", name="homepage")
	 *
	 * Renders homepage (lists categories).
	 */
	public function indexPage() {
		//get root (app/Resources) folder
		$root_folder = $this->get( 'kernel' )->getRootDir();

		//load and decode .json file
		$decoded_json = json_decode( file_get_contents( $root_folder . "/Resources/categories.json" ), true );

		//render template
		return $this->render( 'public/index.html.twig', array(
			'categories' => $decoded_json
		) );
	}


	/**
	 * @Route("/subscription", name="subscription")
	 *
	 * Subscription page.
	 * Lets users subscribe to newsletter. Type in name, email, select categories, etc.
	 *
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function subscriptionPage( Request $request ) {

		//get root (app/Resources) folder
		$root_folder = $this->get( 'kernel' )->getRootDir();

		//creating form from SubscriptionFormType class
		$form = $this->createForm( SubscriptionFormType::class );

		//handling form request on POST
		$form->handleRequest( $request );

		//if form was submited (user submited the registeration form) and form passed validation
		if ( $form->isSubmitted() && $form->isValid() ) {

			//get all POST data
			$data = $form->getData();

			//checking if user selected categories
			if ( ! empty( $data['categories'] ) ) {

				//set registration date
				$data['registration_date'] = date( "Y-m-d H:i:s" );

				//load and decode .json file
				$tempArray = json_decode( file_get_contents( $root_folder . "/Resources/subscribers.json" ), true );

				//perform array merge - appending unique 'id' element to the beginning of array
				$data = array( 'id' => uniqid() ) + $data;

				//if subscribers .json contains any subscribers
				if ( ! empty( $tempArray ) ) {
					array_push( $tempArray, $data );
					$jsonData = json_encode( $tempArray, JSON_PRETTY_PRINT );
				} else {
					//if there are no subscribers at all (if subscribers json is empty)
					$jsonData = json_encode( array( 0 => $data ), JSON_PRETTY_PRINT );
				}

				//put(write) encoded .json to file, use LOCK_EX to acquire an exclusive lock
				file_put_contents( $root_folder . '/Resources/subscribers.json', $jsonData, LOCK_EX );

				//add notification about success
				$this->addFlash(
					'notice-success',
					'Sveikiname, sėkmingai užsiregistravote!'
				);
			} else {
				//add notification about error
				$this->addFlash(
					'notice-danger',
					'Klaida! Pasirinkite bent vieną naujienų kategoriją!'
				);
			}

		}

		//render template with form
		return $this->render( 'public/subscription_page.html.twig', array(
			'form' => $form->createView()
		) );
	}


}
