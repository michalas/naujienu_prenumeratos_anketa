<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 20/10/17
 * Time: 08:10
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


/**
 * Class adminController
 *
 * Class owns all administration (admin) part methods
 *
 * @package AppBundle\Controller
 */
class adminController extends Controller {

	//setting data files
	const SUBSCRIBERS_JSON_FILE = 'subscribers.json';
	const CATEGORIES_JSON_FILE = 'categories.json';


	/**
	 * Function used to return needed decoded .json file as array
	 * Mainly used for getting subscribers.json and categories.json
	 *
	 * @param string $file
	 *
	 * @return mixed
	 */
	public function getJson( $file ) {
		//get root (app/Resources) folder
		$root_folder = $this->get( 'kernel' )->getRootDir();

		//load and decode .json file
		$decoded_json = json_decode( file_get_contents( $root_folder . "/Resources/" . $file ), true );

		return $decoded_json;
	}

	/**
	 * Function used to put(write) array as encoded json to .json file
	 *
	 * @param $json
	 * @param $file
	 */
	public function putJson( $json, $file ) {
		//get root (app/Resources) folder
		$root_folder = $this->get( 'kernel' )->getRootDir();

		//put(write) encoded .json to file, use LOCK_EX to acquire an exclusive lock
		file_put_contents( $root_folder . '/Resources/' . $file, json_encode( $json, JSON_PRETTY_PRINT ), LOCK_EX );
	}


	/**
	 * @Route("/admin", name="admin")
	 *
	 * Printing subscribers list (subscribers.json)
	 */
	public function showSubscribersList() {

		//get subscribers .json as array
		$subscribers_json = $this->getJson( self::SUBSCRIBERS_JSON_FILE );

		//if loaded .json is empty throw notice
		if ( empty( $subscribers_json ) ) {
			$this->addFlash(
				'notice',
				'Hey, dar nėra nei vieno prenumeratoriaus!'
			);
		}

		//rendering template
		return $this->render( 'admin/subscribers_list.html.twig', array(
			'subscribers_json' => $subscribers_json
		) );
	}


	/**
	 * @Route("/admin/delete/{id}")
	 *
	 * Delete subscriber.
	 * Actually - removing subscriber from subscribers list (subscribers.json)
	 *
	 * @param $id
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function deleteSubscriber( $id ) {

		//get .json as array
		$selected_json = $this->getJson( self::SUBSCRIBERS_JSON_FILE );

		//searching for existing subscriber in whole json
		$key = array_search( $id, array_column( $selected_json, 'id' ) );

		//if subscriber exists in json array then remove it
		if ( $key !== false ) {

			//if found - unset/remove from whole json array
			unset( $selected_json[ $key ] );

			//reindex json array
			$selected_json = array_values( $selected_json );

			//put json array to file
			$this->putJson( $selected_json, self::SUBSCRIBERS_JSON_FILE );

		} else {
			$this->addFlash(
				'notice',
				'Gana draskytis! ok? katine hackeri nėra tokio vartotojo ID!'
			);
		}

		//loading showSubscribersList() controller
		return $this->showSubscribersList();
	}


	/**
	 * @Route("/admin/edit/{id}")
	 *
	 * Edition of subscriber.
	 * Ability to change subscriber's name, email, categories.
	 *
	 * @param Request $request
	 * @param $id
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function editSubscriber( Request $request, $id ) {

		//get subscribers .json as array
		$subscribers_json = $this->getJson( self::SUBSCRIBERS_JSON_FILE );

		//select categories.json file
		$categories_json_file_name = 'categories.json';

		//get categories .json as array
		$categories_json = $this->getJson( $categories_json_file_name );

		//searching for existing subscriber in whole json
		$key = array_search( $id, array_column( $subscribers_json, 'id' ) );

		//if subscriber exists in json array then update it
		if ( $key !== false ) {

			//if submit method is POST
			if ( $request->isMethod( 'POST' ) ) {

				//get all POST method data
				$data = $request->request->all();

				//dump( $data );

				//checking if subscriber name is not empty
				if ( empty( $data['name'] ) ) {
					//add error notification
					$this->addFlash(
						'danger',
						'Klaida! neįrašėte prenumeratoriaus vardo!'
					);
				} elseif ( empty( $data['email'] ) ) {
					//add error notification
					$this->addFlash(
						'danger',
						'Klaida! neįrašėte prenumeratoriaus el. pašto!'
					);
				} elseif ( ! isset( $data['categories'] ) ) {
					//add error notification
					$this->addFlash(
						'danger',
						'Klaida! Pasirinkite bent vieną naujienų kategoriją!'
					);
				} else {
					$data['categories'] = array_keys( $data['categories'] );

					//adding all new data to current subscribers array
					$subscribers_json[ $key ] = $data;

					//put updated subscriber data array to .json file
					$this->putJson( $subscribers_json, self::SUBSCRIBERS_JSON_FILE );

					//add success notification
					$this->addFlash(
						'success',
						'Vartotojo duomenys buvo sėkmingai atnaujinti.'
					);
					//checking if subscriber has checked any categories


					//checking if subscriber has checked any categories
				}

			}
		} else {

			//if subscriber array key not exist load default showSubscribersList() controller
			return $this->showSubscribersList();
		}

//dump($subscribers_json);
//dump($categories_json);

//rendering template
		return $this->render( 'admin/subscriber_editing.html.twig', array(
			'subscriber_info' => $subscribers_json[ $key ],
			'categories_json' => $categories_json
		) );
	}


	/**
	 * @Route("/admin/categories", name="admin-categories")
	 *
	 * Management of categories.
	 * If accessed without POST then simply renders list of categories (categories.json)
	 * When accessed with POST - has ability to create new categories.
	 *
	 * FIY: Deleting of categories can be done using deleteCategory() method.
	 *
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function manageCategories(
		Request $request
	) {

		//if this method was accessed directly after deletion of category (by deleteCategory() method)
		//we should add notification about successful category delete action
		if ( $request->get( 'action-status' ) == 'deleted' && $request->isMethod( 'GET' ) ) {
			$this->addFlash(
				'success',
				'Kategorija buvo ištrinta!'
			);
		}

		//get categories .json as array
		$categories_json = $this->getJson( self::CATEGORIES_JSON_FILE );

		//generating form for categories management (more: https://symfony.com/doc/current/forms.html)
		$form = $this->createFormBuilder()
		             ->add( 'category_name', TextType::class, [
			             'attr'        => array(
				             'class'       => 'col-lg-7 col-md-12',
				             'placeholder' => 'Įveskite naujos kategorijos pavadinimą ...'
			             ),
			             'required'    => false,
			             'label'       => false,
			             'constraints' => array(
				             new NotBlank( array(
					             'message' => "*Šis laukas negali būti tuščias."
				             ) ),
				             new Length( array(
					             'min'        => 2,
					             'minMessage' => '*Per mažai simbolių. Patikrinkite ar tikrai įvedėte teisingą kategorijos pavadinimą?',
					             'max'        => 100,
					             'maxMessage' => '*Per daug simbolių. Ar tikrai įvedėte teisingą kategorijos pavadinimą?'
				             ) )
			             )

		             ] )
		             ->add( 'submit', SubmitType::class, [
			             'attr'  => array( 'class' => 'btn-primary' ),
			             'label' => 'Pridėti'
		             ] )
		             ->getForm();

		//handling form POST request
		$form->handleRequest( $request );

		//if the form was submitted and passed validation
		if ( $form->isSubmitted() && $form->isValid() ) {

			//get all (submitted by form) data and extract only 'category_name' part
			$data = $form->getData();
			$data = $data['category_name'];

			//check if added category already exist
			if ( array_search( $data, $categories_json ) == false ) {

				//push new category into categories array
				array_push( $categories_json, $data );

				//put updated categories .json array to file
				$this->putJson( $categories_json, self::CATEGORIES_JSON_FILE );

				//add notification about success
				$this->addFlash(
					'success',
					'Nauja kategorija buvo sėkmingai sukurta.'
				);

			} else {
				//add notification about already existing category
				$this->addFlash(
					'danger',
					'Klaida! Tokia kategorija jau yra, pasirinkite kitą pavadinimą!'
				);
			}

		}


		//rendering template
		return $this->render( 'admin/categories.twig', array(
			'categories_json' => $categories_json,
			'form'            => $form->createView(),
		) );
	}


	/**
	 * @Route("/admin/categories/delete/{id}/{category_title}")
	 *
	 * Deleting category.
	 * Actually - removing from categories list (categories.json)
	 * Also removing relationships with subscribers. (Deleting current category
	 * from all subscribers who has those categories selected).
	 *
	 * @param $id
	 * @param $category_title
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function deleteCategory(
		$id, $category_title
	) {

		//get categories .json as array
		$categories_json = $this->getJson( self::CATEGORIES_JSON_FILE );

		//unset/remove category from categories array
		unset( $categories_json[ $id ] );

		//put json array to categories .json file
		$this->putJson( $categories_json, self::CATEGORIES_JSON_FILE );

		//get subscribers .json and decode it to array
		$subscribers_json = $this->getJson( self::SUBSCRIBERS_JSON_FILE );

		//set empty array for later use
		$updated_array = array();

		//looping through all subscribers in subscribers array
		foreach ( $subscribers_json as $item ) {

			//checking if subscribers had selected current(this one, under deletion) category
			$key = array_search( $category_title, $item['categories'] );

			//if subscriber has selected current category then remove it from his categories list
			if ( $key !== false ) {
				unset( $item['categories'][ $key ] );
			}

			//push stripped array as a new one
			array_push( $updated_array, $item );
		}

		//put updated subscriber data array to .json file
		$this->putJson( $updated_array, self::SUBSCRIBERS_JSON_FILE );

		//make redirect to route '/admin/categories' (manageCategories() method)
		return $this->redirectToRoute( 'admin-categories', array( 'action-status' => 'deleted' ), 301 );
	}


}
